﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class PlayOnHit : MonoBehaviour {
	public float maxVolumeForce = 100;
	public AudioClip hitSound;
	// Use this for initialization
	void OnCollisionEnter (Collision c) {
		audio.PlayOneShot(hitSound, c.relativeVelocity.magnitude / maxVolumeForce);
	}
}
