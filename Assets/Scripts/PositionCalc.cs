﻿using UnityEngine;
using System;
using System.Collections;

public class PositionCalc : MonoBehaviour {
	public Transform [] racers;

	public TextMesh [] playerNamesObjs;

	public int playersNumber;
	private Waypoint waypoint;
	private WaypointInfo [] waypointInfo;

	void Start () {
		waypoint = GameObject.FindObjectOfType<Waypoint>();
		// disable unused position meshes 
		for (int i = playerNamesObjs.Length - 1; i >= playersNumber; i--){
			playerNamesObjs[i].gameObject.SetActive(false);
			playerNamesObjs[i] = null;
		}
	}
	
	void LateUpdate () {
		waypointInfo = waypoint.GetPositions(racers);
		for (int i = waypointInfo.Length - playersNumber; i < waypointInfo.Length; i++){
			if (waypointInfo[i] == null) continue;
			if (waypointInfo[i].transform == null) return;
			if (playerNamesObjs[i - (waypointInfo.Length - playersNumber)] != null)
				playerNamesObjs[i - (waypointInfo.Length - playersNumber)].text = waypointInfo[i].transform.name;
		}
	}

	public int GetRacerPosition(Transform racer){
		if (waypointInfo == null){
			waypointInfo = new WaypointInfo[racers.Length];
		}
		waypointInfo = waypoint.GetPositions(racers);
		foreach(var w in waypointInfo){
			if (w.transform == racer) {
				return Array.IndexOf(waypointInfo, w)+1; 
			}
		}
		return 0;
	}
	public void RemoveRacer(Transform r) {
		for (int i = 0; i < racers.Length; i++){
			if (racers[i] == r){
				racers[i] = null;
				for(int j = 0; j < playerNamesObjs.Length; j++){
					if (playerNamesObjs[j] != null){
						playerNamesObjs[j] = null;
						break;
					}
				}
			}
		}
	}
}
