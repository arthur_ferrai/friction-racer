﻿using UnityEngine;
using System.Collections;

public class SelectCarMulti : ChangeSceneButton {

	public int playerNumber;

	protected override void OnButton ()
	{
		GameOptions.SetInt("playerNumber",playerNumber);
		base.OnButton();
	}
}
