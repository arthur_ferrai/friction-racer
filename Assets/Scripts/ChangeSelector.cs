﻿using UnityEngine;
using System.Collections;

public class ChangeSelector : MonoBehaviour {
	public enum direction {left,right};

	public direction directionToGo;

	private ChildSelector selector;
	void Start () {
		selector = GameObject.FindObjectOfType<ChildSelector>();
	}
	
	void OnButton () {
		if (directionToGo == direction.left) {
			selector.PrevCar();
		} else {
			selector.NextCar();
		}
	}
}
