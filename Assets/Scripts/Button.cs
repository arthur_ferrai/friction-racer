﻿using UnityEngine;
using System.Collections;

public class Button : MonoBehaviour {
	public SpriteRenderer normalBG, clickBG;
	public string functionName;
	public bool isBackButton = false;

	void Start () {
		Released ();
	}

	void Update () {
		if(!enabled) return;
		if(isBackButton && Input.GetKey(KeyCode.Escape)) DoButtonAction();
	}
	void Pressed () {
		if(!enabled) return;
		if(normalBG)
			normalBG.enabled = false;
		if(clickBG)
			clickBG.enabled = true;
	}

	void Released () {
		if(!enabled) return;
		if(normalBG)
			normalBG.enabled = true;
		if(clickBG)
			clickBG.enabled = false;
	}

	void DoButtonAction () {
		if(!enabled) return;
		if (!functionName.Equals(string.Empty)) {
			SendMessage(functionName,SendMessageOptions.DontRequireReceiver);
		}
	}
	//Mouse support
	void OnMouseEnter () {
		Pressed ();
	}
	
	void OnMouseUp () {
		DoButtonAction();
	}
	
	void OnMouseExit () {
		Released ();
	}
}
