﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(Notes))]
public class NotesEditor : Editor {
	private Notes n;
	private GUIStyle style;
	private Vector2 scroll;
	public override void OnInspectorGUI(){
		if (n == null) n = (Notes)target;
		if (style == null){
			style = new GUIStyle();
			style.wordWrap = true;
			style.stretchWidth = false;
		}
		scroll = EditorGUILayout.BeginScrollView(scroll,GUILayout.Height(100.0f));
		n.text = EditorGUILayout.TextArea(n.text,style,GUILayout.Width(200.0f),GUILayout.ExpandWidth(true),GUILayout.ExpandHeight(true));
		EditorGUILayout.EndScrollView();
	}
}
