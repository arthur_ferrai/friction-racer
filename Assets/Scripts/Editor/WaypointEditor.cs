﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(Waypoint))]
public class WaypointEditor : Editor {
	Tool LastTool = Tool.None;
	GUIStyle style = new GUIStyle();
	void OnEnable() {
		LastTool = Tools.current;
		if (Tools.current != Tool.View) Tools.current = Tool.None;
		style.normal.textColor = Color.white;
		((Waypoint)target).transform.hideFlags = HideFlags.NotEditable | HideFlags.HideInInspector;
	}
	void OnDisable() {
		Tools.current = LastTool;
	}
	void OnSceneGUI () {
		if (Tools.current != Tool.View) Tools.current = Tool.None;
		Waypoint w = (Waypoint) target;
		if (w.pointList == null) return;
		//Handles.DrawPolyLine(w.pointList);
		Undo.RecordObject(target,"waypoint change");
		for ( int i = 0; i < w.pointList.Length; i++ ) {
			Handles.Label(w.pointList[i],i.ToString(),style);
			w.pointList[i] = Handles.PositionHandle(w.pointList[i],Quaternion.identity);
			if (GUI.changed){
				EditorUtility.SetDirty(target);
			}
		}
		if (w.loop) {
			//Handles.DrawLine(w.pointList[0],w.pointList[w.pointList.Length-1]);
		}
	}
	public override void OnInspectorGUI ()
	{
		Waypoint w = (Waypoint) target;
		w.loop = EditorGUILayout.Toggle("Is it a loop waypoint?", w.loop);
		EditorGUILayout.Separator();
		if (GUILayout.Button("Add waypoint at start")){
			ArrayUtility.Insert(ref w.pointList,0,(w.pointList.Length == 0) ? Vector3.zero:w.pointList[0]);
			EditorUtility.SetDirty(target);
		}
		for (int i = 0; i < w.pointList.Length; i++){
			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.LabelField("Pos " + i + ":",GUILayout.Width(40));
			w.pointList[i] = EditorGUILayout.Vector3Field("",w.pointList[i]);
			if (GUILayout.Button("+",GUILayout.Width(20))){
				ArrayUtility.Insert(ref w.pointList,i+1,w.pointList[i]);
				EditorUtility.SetDirty(target);
			}
			if (GUILayout.Button("-",GUILayout.Width(20))){
				ArrayUtility.RemoveAt(ref w.pointList,i);
				EditorUtility.SetDirty(target);
			}
			EditorGUILayout.EndHorizontal();
		}

		if (GUI.changed){
			EditorUtility.SetDirty(target);
		}
	}
}
