﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using UnityEditorInternal;
using System.Reflection;
using System;

public class SelectLayer : EditorWindow {

	[MenuItem("GameObject/Select With Tag")]
	static void Init () {
		SelectLayer window = (SelectLayer)EditorWindow.GetWindow (typeof (SelectLayer));
	}
	
	int selected;
	void OnGUI () {
		selected = EditorGUILayout.LayerField(selected);
		if (GUILayout.Button("Select")) {
			GameObject [] selectedObj = GameObject.FindObjectsOfType<GameObject>();
			List<GameObject> ret = new List<GameObject>();
			foreach(var obj in selectedObj){
				if (obj.layer == selected)
					ret.Add(obj);
			}
			Selection.objects = ret.ToArray();
		}
	}
}
