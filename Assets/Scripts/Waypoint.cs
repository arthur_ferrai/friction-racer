﻿using UnityEngine;
using System;
using System.Collections;
public class WaypointInfo {
	public Transform transform;
	public int waypoint;
	public float distance;
};

public class Waypoint : MonoBehaviour {
	public Vector3 [] pointList;
	public bool loop;
	public WaypointInfo [] GetPositions(params Transform [] values){
		WaypointInfo [] ret = new WaypointInfo[values.Length];
		for(int i = 0; i < values.Length; i++){
			if (values[i] == null) continue;
			ret[i] = new WaypointInfo();
			for(int j = 1; j < pointList.Length; j++){
				if ((pointList[j] - pointList[j-1]).sqrMagnitude > (values[i].position - pointList[j-1]).sqrMagnitude) {
					ret[i].transform = values[i];
					ret[i].waypoint = j-1;
					ret[i].distance = Vector3.Dot(pointList[j] - pointList[j-1], 
					                              values[i].position - pointList[j-1]);
					break;
				}
			}
		}
		Array.Sort(ret,Waypoint.SortRule);
		return ret;
	}

	// 1 for left is greater (left go right)
	//-1 for right is greater(right go right)
	// 0 for equal
	private static int SortRule(WaypointInfo left, WaypointInfo right) {
		if (left == null) {
			if (right == null) { //both null, both equal
				return 0;
			} else { // only left null, right go right
				return -1;
			}
		} else { // left not null
			if (right == null) { //right null, left go right
				return 1;
			} else { // no one is null, let's analyse
				if (left.waypoint == right.waypoint) { // both are on the same waypoint
					if (left.distance > right.distance) { // left is further than right, right go right
						return -1;
					} else if (left.distance < right.distance) { // right is further than left, left go right
						return 1;
					} else return 0; // both on same place, on same waypoint
				} else { // waypoints are not the same, let's compare them
					if (left.waypoint > right.waypoint) { // left is further than right, right go right
						return -1;
					} else { // right is further than left, left go right
						return 1;
					}
				}
			}
		}
	}
	void OnDrawGizmos() {
		for ( int i = 1; i < pointList.Length; i++ ) {
			Gizmos.DrawLine(pointList[i-1],pointList[i]);
		}
		if (loop) Gizmos.DrawLine(pointList[0],pointList[pointList.Length-1]);
	}
}
