﻿using UnityEngine;
using System.Collections;

public class ChildSelector : MonoBehaviour {
	public float degreesPerSecond = 180;
	public string persistentDataName = "";
	private GameObject [] childs;
	private int selected = 0;
	public int Selected{
		get{
			return selected;
		}
	}
	void Awake () {
		int player = GameOptions.GetInt("playerNumber");
		if (player != -1) persistentDataName = string.Format("{0} {1}",persistentDataName,player);
		selected = GameOptions.GetInt(persistentDataName);
		if (selected == -1) selected = 0;
	}
	void Start () {
		childs = new GameObject[transform.childCount];
		int i = 0;
		foreach (Transform t in transform) {
			childs[i] = t.gameObject;
			t.gameObject.SetActive(false);
			i++;
		}
		childs[selected].SetActive(true);
	}
	void Update () {
		transform.Rotate(Vector3.up,degreesPerSecond * Time.unscaledDeltaTime);
	}
	public void NextCar () {
		childs[selected].SetActive(false);
		selected ++;
		if (selected == transform.childCount)
			selected = 0;
		childs[selected].SetActive(true);
	}
	public void PrevCar () {
		childs[selected].SetActive(false);
		selected --;
		if (selected < 0)
			selected = transform.childCount - 1;
		childs[selected].SetActive(true);
	}
	public void SaveOption () {
		GameOptions.SetInt(persistentDataName,selected);
		GameOptions.Wipe("playerNumber");
		GameOptions.Save();
	}
}
