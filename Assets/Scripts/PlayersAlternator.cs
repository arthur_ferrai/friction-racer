﻿using UnityEngine;
using System.Collections;

public class PlayersAlternator : MonoBehaviour {
	public TextMesh timerText;
	public int timeToAlternate = 20;
	public IAddTorque [] carsControllers;
	public EndingCameraFly ending;

	private int currentTime;
	private int activeCar = -1;
	private CameraFollow carCam;
	private MessagePopup popup;
	void Start () {
		// get camera that follows the car
		carCam = FindObjectOfType<CameraFollow>();
		popup = GameObject.FindObjectOfType<MessagePopup>();
		// make sure only the first one is active
		//carsControllers[0].enabled = true;
		for (int i = 0; i < carsControllers.Length; i++){
			if(carsControllers[i] != null)
				carsControllers[i].enabled = false;
		}
		carCam.target = carsControllers[0].transform;
	}
	IEnumerator Timer(){
		timerText.text = currentTime.ToString();
		while(currentTime > 0){
			yield return new WaitForSeconds(Time.timeScale);
			currentTime--;
			timerText.text = currentTime.ToString();
		}
		Alternate();
		ResetTimer();
	}
	int counter = 0;
	public void Alternate() {
		counter++;
		if (counter >= carsControllers.Length){
			return;
		}
		// turn off controller of selected car
		if (carsControllers[activeCar] != null)
			carsControllers[activeCar].enabled = false;
		activeCar++;
		if (activeCar == carsControllers.Length) {
			activeCar = 0;
		}
		// get next if not active
		if (carsControllers[activeCar] == null){Alternate();return;}
		// turn on controller of next car only if it is not the last active car
		int countNullCars = 0;
		for (int i = 0; i < carsControllers.Length; i++) {
			if(carsControllers[i] == null) countNullCars++;
		}
		if (countNullCars < carsControllers.Length - 1){
			carsControllers[activeCar].enabled = true;
			carCam.target = carsControllers[activeCar].transform;
		}
		counter = 0;

	}
	public void StopTimer(){
		StopCoroutine("Timer");
	}
	public void ResumeTimer(){
		StartCoroutine("Timer");
	}
	public void ResetTimer(){
		if (activeCar == -1) {
			activeCar = 0;
			carsControllers[activeCar].enabled = true;
		}
		StopCoroutine("Timer");
		currentTime = timeToAlternate;
		StartCoroutine("Timer");
	}
	public void RemoveCar (IAddTorque car) {
		car.enabled = false;
		int i = 0;
		int deactivatedAmount = 0;
		for (; i < carsControllers.Length; i++){
			if (carsControllers[i] == null || carsControllers[i] == car) {
				carsControllers[i] = null;
				deactivatedAmount++;
			}
		}
		if (deactivatedAmount == carsControllers.Length - 1) {
			iTween.CameraFadeTo(1,1*Time.timeScale);
			Invoke("EndGame",1.2f*Time.timeScale);
			return;
		}
		popup.DrawMessage(text: string.Format("{0} Finished!",car.name), from: MessageDirection.left, to: MessageDirection.right);
	}
	public void EndGameNow() {
		for (int i = 0; i < carsControllers.Length; i++){
			carsControllers[i] = null;
		}
		iTween.CameraFadeTo(1,1*Time.timeScale);
		Invoke("EndGame",1.2f*Time.timeScale);
		return;
	}
	void EndGame() {
		ending.gameObject.SetActive(true);
	}
	/*void OnGUI(){
		if(GUILayout.Button("Alternate")){
			Alternate();
		}
	}*/
}
