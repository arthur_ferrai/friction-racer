﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AttributeRender : MonoBehaviour {
	public int attributePoints;
	public int maxAttributePoints = 21;
	public float distanceBetweenPoints;
	public Sprite activePoint, inactivePoint;
	public int sortingOrder;
	private List<SpriteRenderer> points;

	void Start () {
		float h = activePoint.rect.height, w = activePoint.rect.width;
		points = new List<SpriteRenderer> ();
		for (int i = 0; i < maxAttributePoints; i++) {
			GameObject go = new GameObject();
			SpriteRenderer r = go.AddComponent<SpriteRenderer>() as SpriteRenderer;
			// put right sprite
			r.sprite = (i < attributePoints) ? activePoint : inactivePoint;
			r.sortingOrder = sortingOrder;
			go.transform.parent = transform;
			go.transform.localPosition = new Vector3((w+distanceBetweenPoints)*i + w/2,h - h/2);
			go.name = "Point";
			points.Add(r);
		}
	}

	public void SetAttributePoints (int val) {
		if (val > maxAttributePoints || val < 0) return;
		if (val > attributePoints) {
			for (int i = attributePoints; i < val; i++) {
				points [i].sprite = activePoint;
			}
		} else {
			for (int i = val; i < attributePoints; i++) {
				points [i].sprite = inactivePoint;
			}
		}
		attributePoints = val;
	}
}
