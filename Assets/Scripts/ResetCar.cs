﻿using UnityEngine;
using System.Collections;

public class ResetCar : MonoBehaviour {
	public float upperHeight = 1;
	public float time = 1;
	private Waypoint w;
	void Awake () {
		time = time*Time.timeScale;
		w = GameObject.FindObjectOfType<Waypoint>();
	}
	public void ResetObj(bool smooth) {
		int selectedWaypoint = FindBestWaypoint();
		Vector3 pos = Vector3.Project(transform.position - w.pointList[selectedWaypoint],w.pointList[selectedWaypoint + 1] - w.pointList[selectedWaypoint]);
		Vector3 finalPoint = pos + w.pointList[selectedWaypoint];
		if (smooth) {
			Vector3 delta = finalPoint - transform.position;
			Vector3 upperPoint = transform.position + delta/2 + Vector3.up * upperHeight;
			iTween.MoveTo(gameObject,
			              iTween.Hash(
						      "time",time,
							  "path",new Vector3[]{transform.position,upperPoint,finalPoint}
						  ));
			iTween.LookTo(gameObject,w.pointList[selectedWaypoint+1] + delta*-1,time);
		} else {
			transform.position = finalPoint;
			transform.LookAt(w.pointList[selectedWaypoint+1]);
			bool hitCar;
			do {
				hitCar = false;
				RaycastHit [] allHits = rigidbody.SweepTestAll(Vector3.down);
				for (int k = 0; k < allHits.Length; k++) {
					if (allHits[k].collider.gameObject.layer == LayerMask.NameToLayer("Car")){
						hitCar = true;
						break;
					}
				}
				if (hitCar) {
					transform.Translate(-transform.forward*10);
				}
			} while(hitCar);
		}
	}
	int FindBestWaypoint() {
		Vector3 currentPos = transform.position;
		int selectedWaypoint = 0;
		for (int i = 1; i < w.pointList.Length; i++) {
			if (Vector3.Distance(currentPos, w.pointList[i]) < Vector3.Distance(currentPos, w.pointList[selectedWaypoint]))
				selectedWaypoint = i;
		}
		return selectedWaypoint;
	}
	/*void OnGUI() {
		GUILayout.Space(20);
		if (GUILayout.Button("Reset"))
			ResetObj(false);
		if (GUILayout.Button("Reset Smooth"))
			ResetObj(true);
	}*/
}
