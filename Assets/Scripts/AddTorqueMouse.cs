﻿using UnityEngine;
using System.Collections;

public class AddTorqueMouse : IAddTorque {

	public Projector distanceBlob;
	public float minDistanceToRotate = 1;
	public float maxDistanceToDrag = 8;
	public Transform backLeft, backRight, frontLeft, frontRight;
	private Vector3 startPos;
	private CameraFollow camFollow;

	protected override void Awake(){
		base.Awake();
		distanceBlob = transform.Find("Distance Indicator").GetComponent<Projector>();
		backLeft   = transform.Find("roda_t_e");
		backRight  = transform.Find("roda_t_d");
		frontRight = transform.Find("roda_d_d");
		frontLeft  = transform.Find("roda_d_e");

		distanceBlob.orthographicSize = maxDistanceToDrag;
		camFollow = Camera.main.GetComponent<CameraFollow> ();
		distanceBlob.gameObject.SetActive(true);
		distanceBlob.enabled = false;

	}
	GameObject moveObj;
	void LateUpdate() {
		distanceBlob.transform.position = startPos;
		if (moveObj != null){
			moveObj.hingeJoint.axis = transform.forward;
		}
	}
	void OnMouseDown() {
		if(!enabled) return;
		alternator.StopTimer();
		startPos = transform.position;
		camFollow.enabled = false;
		distanceBlob.enabled = true;
		if (moveObj != null) return;
		moveObj = new GameObject("TouchPos",typeof(Rigidbody));
		moveObj.transform.position = transform.position;
		//SpringJoint joint = moveObj.AddComponent<SpringJoint>();
		HingeJoint joint = moveObj.AddComponent<HingeJoint>();
		joint.axis = Vector3.forward;
		moveObj.rigidbody.isKinematic = true;
		moveObj.rigidbody.mass=rigidbody.mass;
		joint.connectedBody = rigidbody;
		//joint.damper = 1000;
		//joint.spring = 2000;
		joint.enableCollision=false;
		moveObj.transform.Translate(Vector3.up);
		rigidbody.freezeRotation = true;
	}

	RaycastHit rayInfo;

	RaycastHit lr, rr, lf, rf;
	Vector3 upDir;
	float initHeight = 0;
	float lastDistance = 0;
	void OnMouseDrag() {
		if(!enabled) return;
		float currentDistance = Vector3.Distance(transform.position,startPos)/maxDistanceToDrag;
		if(currentDistance > lastDistance + 0.05f){
			audio.PlayOneShot(springSound, currentDistance + 0.1f);
			lastDistance = currentDistance;
		} else if (currentDistance < lastDistance) {
			lastDistance = currentDistance;
		}
		Ray r = Camera.main.ScreenPointToRay (Input.mousePosition);
		Physics.Raycast(startPos,Vector3.down,out rayInfo, 10000, ~(LayerMask.GetMask("Car","Ignore Raycast")));
		initHeight = rayInfo.distance;
		
		// find out where to drag car
		Physics.Raycast (r, out rayInfo, 10000, ~(LayerMask.GetMask("Car","Ignore Raycast")));
		Vector3 targetPos = rayInfo.point + Vector3.up * (initHeight);
		float dist = (targetPos - startPos).magnitude;
		moveObj.transform.position = startPos + Vector3.ClampMagnitude(targetPos - startPos,maxDistanceToDrag);//+Vector3.up;
		if (dist >= minDistanceToRotate) {
			Vector3 rotationAngle = transform.rotation.eulerAngles;
			Quaternion lookQuat = Quaternion.LookRotation (startPos - targetPos, upDir.normalized);
			lookQuat.eulerAngles = new Vector3 (rotationAngle.x,lookQuat.eulerAngles.y,rotationAngle.z);
			transform.rotation = lookQuat;
		}
	}
	void OnMouseUp() {
		if(!enabled) return;
		rigidbody.freezeRotation = false;
		Destroy(moveObj);
		car.AddTorque ((transform.position - startPos).magnitude*5);
		camFollow.enabled = true;
		distanceBlob.enabled=false;
		StartCoroutine(EndTurn());
	}
}
