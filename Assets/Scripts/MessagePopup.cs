﻿using UnityEngine;
using System.Collections;
public enum MessageDirection{left = 1<<0, right = 1<<1, up = 1<<2, down = 1<<3};
public class MessagePopup : MonoBehaviour {
	private TextMesh textMesh;
	void Awake() {
		textMesh = gameObject.GetComponent<TextMesh>();
	}
	public void DrawMessage(string text, float timeOnScreen = 1f, MessageDirection from = MessageDirection.up, MessageDirection to = MessageDirection.down, float delayBeforeStart = 0) {
		textMesh.text = text;
		Vector3 startPos = new Vector3( (-Mathf.Clamp01((int)(from & MessageDirection.left)) + Mathf.Clamp01((int)(from & MessageDirection.right))) * 800,
		                                (-Mathf.Clamp01((int)(from & MessageDirection.down)) + Mathf.Clamp01((int)(from & MessageDirection.up)))    * 1000);

		Vector3 endPos   = new Vector3( (-Mathf.Clamp01((int)(to & MessageDirection.left)) + Mathf.Clamp01((int)(to & MessageDirection.right))) * 800,
		                                (-Mathf.Clamp01((int)(to & MessageDirection.down)) + Mathf.Clamp01((int)(to & MessageDirection.up)))    * 1000);
		transform.position = startPos;
		iTween.Stop(gameObject);
		// start movement
		iTween.MoveTo(gameObject, iTween.Hash("position", Vector3.zero, 
		                                      "time", 	  0.5f*Time.timeScale,
		                                      "delay" ,   delayBeforeStart*Time.timeScale,
		                                      "easetype", iTween.EaseType.easeOutQuint));
		// end movement
		iTween.MoveTo(gameObject, iTween.Hash("position", endPos,
		                                      "time",     0.5f*Time.timeScale,
		                                      "easetype", iTween.EaseType.easeInQuint,
		                                      "delay",    (delayBeforeStart + timeOnScreen + 0.5f)*Time.timeScale));
	}
}
