﻿using UnityEngine;
using System.Collections;

public class PrepareScene : MonoBehaviour {
	public int placeholdersSize = 2;
	public Vector3 [] carPlaces;
	public Transform [] cars;
	public AudioClip springSound;
	private PositionCalc pos;
	private PlayersAlternator altern;
	private CameraFollow cam;
	private int playersNumber = 0;
	void Awake () {
		cam    = GameObject.FindObjectOfType<CameraFollow>();
		altern = GameObject.FindObjectOfType<PlayersAlternator>();
		pos    = GameObject.FindObjectOfType<PositionCalc>();

		Transform [] instantiatedCars = new Transform[4];
		if(GameOptions.GetInt("GameType") == 0) { //single player
			// single player will always have 4 players
			playersNumber = 4;
			// create player car
			int selectedCar = GameOptions.GetInt("Car Selector");
			if (selectedCar < 0) selectedCar = 0;
			instantiatedCars[0] = Instantiate(cars[selectedCar],carPlaces[0], Quaternion.identity) as Transform;
			PrepareTorqueScript(instantiatedCars[0].gameObject.AddComponent<AddTorqueMouse>());
			instantiatedCars[0].name = "Player1";
			// create CPU cars
			for(int i = 1; i < 4; i++) {
				instantiatedCars[i] = Instantiate(cars[Random.Range(0,7)],carPlaces[i],Quaternion.identity) as Transform;
				PrepareTorqueScript(instantiatedCars[i].gameObject.AddComponent<AddTorqueCPU>());
				instantiatedCars[i].name = string.Format("CPU {0}",i);
			}
		} else { // multi player
			for(int i = 0; i < 4; i++) {
				int selectedCar = GameOptions.GetInt(string.Format("Car Selector {0}",i+1));
				if (selectedCar < 0) selectedCar = 0;
				if(i <= 1 || GameOptions.GetInt(string.Format("Player{0}Active",i+1)) == 1){
					instantiatedCars[i] = Instantiate(cars[selectedCar],carPlaces[i],Quaternion.identity) as Transform;
					PrepareTorqueScript(instantiatedCars[i].gameObject.AddComponent<AddTorqueMouse>());
					instantiatedCars[i].name = string.Format("Player{0}",i+1);
					playersNumber++;
				}
			}
		}
		instantiatedCars[0].GetComponent<IAddTorque>().enabled = true;
		// initialize control objects
		altern.carsControllers = new IAddTorque[4];
		for(int i = 0; i < 4; i++){
			if(instantiatedCars[i] != null)
				altern.carsControllers[i] = instantiatedCars[i].GetComponent<IAddTorque>();
		}
		pos.racers = instantiatedCars;
		pos.playersNumber = playersNumber;
		cam.target = instantiatedCars[0];

	}
	void PrepareTorqueScript(IAddTorque t){
		t.enabled = false;
		t.springSound = springSound;
	}
	void OnDrawGizmos() {
		Gizmos.color = Color.blue;
		Gizmos.DrawCube(carPlaces[0],Vector3.one*placeholdersSize);
		Gizmos.color = Color.green;
		Gizmos.DrawCube(carPlaces[1],Vector3.one*placeholdersSize);
		Gizmos.color = Color.red;
		Gizmos.DrawCube(carPlaces[2],Vector3.one*placeholdersSize);
		Gizmos.color = Color.yellow;
		Gizmos.DrawCube(carPlaces[3],Vector3.one*placeholdersSize);
	}
}
