﻿using UnityEngine;
using System.Collections.Generic;

public static class GameOptions {
	private static bool dataLoaded = false;
	public static int GetInt (string name) {
		if (!dataLoaded) Load();
		return PlayerPrefs.GetInt(name,-1);
	}
	public static float GetFloat (string name) {
		if (!dataLoaded) Load();
		return PlayerPrefs.GetFloat(name,-1);
	}
	public static string GetString (string name) {
		if (!dataLoaded) Load();
		return PlayerPrefs.GetString(name,"");
	}
	public static void SetInt (string name, int val) {
		if (!dataLoaded) Load();
		PlayerPrefs.SetInt(name, val);
	}
	public static void SetFloat (string name, float val) {
		if (!dataLoaded) Load();
		PlayerPrefs.SetFloat(name, val);
	}
	public static void SetString (string name, string val) {
		if (!dataLoaded) Load();
		PlayerPrefs.SetString(name, val);
	}
	// clear all saved values
	public static void Wipe () {
		if (!dataLoaded) Load();
		PlayerPrefs.DeleteAll();
	}
	// clear a particular value
	public static void Wipe (string name) {
		if (!dataLoaded) Load();
		PlayerPrefs.DeleteKey(name);
	}
	// persist data
	public static void Save () {
		PlayerPrefs.Save();
	}
	// get data from whatever it is stored
	private static void Load () {
		// do code to get data
		dataLoaded = true;
	}
}
