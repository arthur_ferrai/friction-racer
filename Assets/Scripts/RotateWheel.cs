﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(WheelCollider))]
public class RotateWheel : MonoBehaviour {

	public Transform wheelMesh;
	private WheelCollider col;

	void Awake (){
		if (!wheelMesh) Destroy(this);
		col = GetComponent<WheelCollider>();
	}
	void Update () {
		wheelMesh.Rotate((col.rpm*6)*Time.deltaTime,0,0);
	}
}
