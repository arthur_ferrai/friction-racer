﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(FrictionCar))]
public abstract class IAddTorque : MonoBehaviour {
	public AudioClip springSound;
	protected FrictionCar car;
	protected PlayersAlternator alternator;
	private bool firstRun = true;
	private ResetCar r;
	protected MessagePopup popup;
	protected bool canPlay = false;
	protected virtual void Awake() {
		r = GetComponent<ResetCar>();
		car = GetComponent<FrictionCar>();
		alternator = GameObject.FindObjectOfType<PlayersAlternator>();
		popup = GameObject.FindObjectOfType<MessagePopup>();
	}
	void Start() {
		canPlay = true;
		OnEnable();
	}
	protected virtual void OnEnable() {
		if (canPlay){
			if (!firstRun){
				r.ResetObj(false);
			}
			alternator.ResetTimer();
			popup.DrawMessage(string.Format("{0} Turn!", name));
		}
	}
	protected IEnumerator EndTurn() {
		firstRun = false;
		enabled = false;
		yield return new WaitForSeconds(Time.timeScale);
		int timeout = 5;
		while (!(rigidbody.velocity.sqrMagnitude < 0.001f) && timeout > 0){
			yield return new WaitForSeconds(1);
			timeout--;
		}
		yield return new WaitForSeconds(Time.timeScale);
		alternator.Alternate();
	}
}
