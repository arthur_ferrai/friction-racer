﻿using UnityEngine;
using System.Collections;

public class SaveAndGoToScreenButton : ChangeSceneButton {
	private ChildSelector selector;
	// Use this for initialization
	void Start () {
		selector = GameObject.FindObjectOfType<ChildSelector>();
	}
	
	protected override void OnButton() {
		selector.SaveOption();
		base.OnButton();
	}
}
