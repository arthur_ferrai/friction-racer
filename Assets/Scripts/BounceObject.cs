﻿using UnityEngine;

public class BounceObject : MonoBehaviour {
	public Vector2 startPos;
	public Vector2 reverseEndPos = Vector2.zero;
	public float delay;
	public float reverseDelay;
	public float time = 1;
	public iTween.EaseType ease = iTween.EaseType.easeOutBack;
	public iTween.EaseType reverseEase = iTween.EaseType.easeInBack;
	Vector3 endPos;
	// Use this for initialization
	void Start () {
		endPos = transform.localPosition;
		transform.localPosition = (Vector3)startPos + Vector3.forward*endPos.z;
		iTween.MoveTo (gameObject, iTween.Hash ("position",endPos,"easetype",ease,"delay",delay*Time.timeScale,"time",time*Time.timeScale,"islocal",true));
	}

	public void Reverse () {
		if (reverseEndPos.sqrMagnitude == 0) reverseEndPos = startPos;
		iTween.MoveTo (gameObject, iTween.Hash ("position",(Vector3)reverseEndPos + Vector3.forward*endPos.z,"easetype",reverseEase,"time",time*Time.timeScale, "delay",reverseDelay*Time.timeScale,"islocal",true));
	}
}
