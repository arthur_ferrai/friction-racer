﻿using UnityEngine;
using System.Collections;

public class ChangeSceneButton : MonoBehaviour {
	public string sceneToLoad;

	protected virtual void OnButton () {
		if (sceneToLoad.Equals(string.Empty)){
			Debug.LogError("Will not load any scene!");
			return;
		}
		StartCoroutine("DoLoadNextLevel");
	}
	IEnumerator DoLoadNextLevel(){
		foreach(var button in GameObject.FindObjectsOfType<Button>())
			button.enabled = false;
		var buttons = GameObject.FindObjectsOfType<BounceObject>();
		foreach(var button in buttons){
			button.Reverse();
		}
		yield return new WaitForSeconds(1*Time.timeScale);
		//while ( iTween.Count() > 0)
		//	yield return new WaitForSeconds(1);
		Application.LoadLevel(sceneToLoad);
	}
}
