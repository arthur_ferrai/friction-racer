﻿using UnityEngine;
using System.Collections;

public class TogglePlayer : MonoBehaviour {
	public int playerNumber;
	private Transform on,off;
	private bool status;
	void Awake(){
		on  = transform.Find("On");
		off = transform.Find("Off");
		status = (GameOptions.GetInt(string.Format("Player{0}Active",playerNumber)) == 1);
		UpdateStatus();
	}
	void OnButton () {
		status = GameOptions.GetInt(string.Format("Player{0}Active",playerNumber)) == 0 ? true : false;
		GameOptions.SetInt(string.Format("Player{0}Active",playerNumber),status?1:0);
		UpdateStatus();
	}
	void UpdateStatus(){
		on.gameObject.SetActive(status);
		off.gameObject.SetActive(!status);
	}
	void OnDestroy(){
		GameOptions.Save();
	}
}
