﻿using UnityEngine;
using System.Collections;

public enum GameType{single,multi};
public class PlayGameButton : MonoBehaviour {
	public GameType gameType;
	public int sceneCountOffset;

	void OnButton () {
		GameOptions.SetInt("GameType",(int)gameType);
		Application.LoadLevel(GameOptions.GetInt("Course Selector") + sceneCountOffset);
	}
}
