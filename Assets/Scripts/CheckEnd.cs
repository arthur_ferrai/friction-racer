﻿using UnityEngine;
using System.Collections;

public class CheckEnd : MonoBehaviour {
	private PlayersAlternator a;
	private PositionCalc c;
	// Use this for initialization
	void Start () {
		a = GameObject.FindObjectOfType<PlayersAlternator>();
		c = GameObject.FindObjectOfType<PositionCalc>();
	}
	
	void OnTriggerEnter (Collider other) {
		// check if the trigger is in EndMark layer
		if (other.gameObject.layer == LayerMask.NameToLayer("EndingMark")) {
			if ((GameOptions.GetInt("GameType") == (int)GameType.single) && gameObject.GetComponent<AddTorqueMouse>() != null){
				a.EndGameNow();
				c.RemoveRacer(transform);
			} else {
				a.RemoveCar(GetComponent<IAddTorque>());
				c.RemoveRacer(transform);
			}
			Destroy(this);
		}
	}
}
