﻿using UnityEngine;
using System.Collections;

public class PlayButtonSound : MonoBehaviour {
	public AudioClip soundEffect;

	void Awake () {
		if(audio == null) {
			gameObject.AddComponent<AudioSource>();
		}
		audio.clip = soundEffect;
		audio.playOnAwake = false;
	}
	
	public void OnButton () {
		audio.Play();
	}
}
