﻿using UnityEngine;
using System.Collections;

public class IntroCameraFly : MonoBehaviour {
	public float timeToFadeIn = 1;
	public float fadeOutStart = 16;

	public TextMesh levelName;

	private PlayersAlternator a;

	void Start () {
		a = GameObject.FindObjectOfType<PlayersAlternator>();
		levelName.text = Application.loadedLevelName;
		iTween.CameraFadeAdd();
		iTween.CameraFadeFrom(1,timeToFadeIn*Time.timeScale);
		iTween.MoveTo(levelName.gameObject,iTween.Hash("x",0,
		                                               "y",0,
		                                               "delay",timeToFadeIn*Time.timeScale,
		                                               "time",2*Time.timeScale,
		                                               "easetype",iTween.EaseType.easeOutExpo,
		                                               "islocal",true));
		iTween.MoveTo(levelName.gameObject,iTween.Hash("x",-80,
		                                    		   "y",0,
		                                               "delay",timeToFadeIn*5*Time.timeScale,
		                                               "time",2*Time.timeScale,
		                                               "easetype",iTween.EaseType.easeInExpo,
		                                               "islocal",true));
		Invoke("DoFadeOut",fadeOutStart*Time.timeScale);
	}
	void DoFadeOut(){
		iTween.CameraFadeTo(iTween.Hash("amount",1,
		                                "time",timeToFadeIn*Time.timeScale,
		                                "oncomplete","EndIntro",
		                                "oncompletetarget",gameObject));
	}
	void Update() {
		if(Input.touchCount > 0 || Input.GetMouseButton(0)){
			CancelInvoke();
			DoFadeOut();
		}
	}
	public void EndIntro () {
		Destroy(gameObject);
		iTween.CameraFadeTo(iTween.Hash("amount",0,
		                                "time",timeToFadeIn*Time.timeScale));
		a.ResetTimer();
	}
}
