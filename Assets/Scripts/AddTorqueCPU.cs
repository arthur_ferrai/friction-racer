﻿using UnityEngine;
using System.Collections;

public class AddTorqueCPU : IAddTorque {

	public float minDistanceToRotate = 1;

	public float maxDistanceToDrag = 8;

	public Transform backLeft, backRight, frontLeft, frontRight;

	private Vector3 startPos;
	private CameraFollow camFollow;

	private float initHeight = 0;
	private RaycastHit rayInfo;


	protected override void Awake(){
		base.Awake();
		Projector p = transform.Find("Distance Indicator").GetComponent<Projector>();
		if (p != null) p.enabled = false;

		backLeft   = transform.Find("roda_t_e");
		backRight  = transform.Find("roda_t_d");
		frontRight = transform.Find("roda_d_d");
		frontLeft  = transform.Find("roda_d_e");

		camFollow = Camera.main.GetComponent<CameraFollow> ();
	}

	protected override void OnEnable() {
		base.OnEnable();
		if(canPlay){
			//StartCoroutine(StartTurn());
			Invoke("StartTurn",3*Time.timeScale);
		}
	}
	GameObject moveObj;
	void LateUpdate() {
		if (moveObj != null) {
			moveObj.hingeJoint.axis = transform.forward;
		}
	}
	void StartTurn(){
		startPos = transform.position;
		camFollow.enabled = false;
		Waypoint w = GameObject.FindObjectOfType<Waypoint>();
		// get waypoint target to aim
		Transform t = transform;
		Vector3 goToPoint = t.position;
		for (int i = w.pointList.Length - 1; i > 0; i--) {
			Vector3 dir = w.pointList[i] - t.position;
			RaycastHit info;
			if (!Physics.Raycast(t.position,dir,out info, dir.magnitude,~(1<<8))){;
				goToPoint = t.position - (w.pointList[i] - t.position);
				break;
			}
			//yield return null;
		}
		Physics.Raycast(startPos,Vector3.down,out rayInfo, 10000, ~(1<<8));
		initHeight = rayInfo.distance;
		goToPoint.y = startPos.y + initHeight;
		moveObj = new GameObject("TouchPos",typeof(Rigidbody));
		moveObj.transform.position = transform.position;
		//SpringJoint joint = moveObj.AddComponent<SpringJoint>();
		HingeJoint joint = moveObj.AddComponent<HingeJoint>();
		joint.axis = Vector3.forward;
		moveObj.rigidbody.isKinematic = true;
		moveObj.rigidbody.mass=rigidbody.mass;
		joint.connectedBody = rigidbody;
		//joint.damper = 1000;
		//joint.spring = 2000;
		joint.enableCollision=false;
		moveObj.transform.Translate(Vector3.up);
		rigidbody.freezeRotation = true;
		alternator.StopTimer();
		iTween.ValueTo(gameObject,iTween.Hash("from",startPos,
		                                      "to",goToPoint,
		                                      "onupdate","CalcCarPos", 
		                                      "onupdatetarget", gameObject, 
		                                      //"oncomplete","EndPlay",
		                                      //"oncompletetarget",gameObject,
		                                      "time",2.0f*Time.timeScale));
		Invoke("EndPlay",1.9f);
	}

	public void EndPlay () {
		Destroy(moveObj);
		camFollow.enabled = true;
		rigidbody.freezeRotation = false;
		car.AddTorque ((transform.position - startPos).magnitude*5);
		StartCoroutine(EndTurn());
	}

	RaycastHit lr, rr, lf, rf;
	Vector3 upDir;
	float lastDistance = 0;
	public void CalcCarPos(Vector3 targetPos) {
		if (moveObj == null) return;
		float currentDistance = Vector3.Distance(transform.position,startPos)/maxDistanceToDrag;
		if(currentDistance > lastDistance + 0.05f){
			audio.PlayOneShot(springSound, currentDistance + 0.1f);
			lastDistance = currentDistance;
		} else if (currentDistance < lastDistance) {
			lastDistance = currentDistance;
		}
		Physics.Raycast (targetPos + Vector3.up*45,Vector3.down, out rayInfo, 10000, ~(LayerMask.GetMask("Car")));

		targetPos = rayInfo.point + Vector3.up * (initHeight + 1f);
		float dist = (targetPos - startPos).magnitude;
		transform.position = startPos + Vector3.ClampMagnitude(targetPos - startPos,maxDistanceToDrag);//+Vector3.up;

	#region carAlign
		// find out the car rotation
		Physics.Raycast(backLeft.position, Vector3.down, out lr,10000,~(1<<8));
		Physics.Raycast(backRight.position, Vector3.down, out rr,10000,~(1<<8));
		Physics.Raycast(frontLeft.position, Vector3.down, out lf,10000,~(1<<8));
		Physics.Raycast(frontRight.position, Vector3.down, out rf,10000,~(1<<8));

		upDir = (Vector3.Cross (rr.point - Vector3.up, lr.point - Vector3.up)+
		         Vector3.Cross (lr.point - Vector3.up, lf.point - Vector3.up)+
		         Vector3.Cross (lf.point - Vector3.up, rf.point - Vector3.up)+
		         Vector3.Cross (rf.point - Vector3.up, rr.point - Vector3.up)
		         ).normalized;
	#endregion
		if (dist >= minDistanceToRotate) {
			Vector3 rotationAngle = transform.rotation.eulerAngles;
			Quaternion lookQuat = Quaternion.LookRotation (startPos - targetPos, upDir.normalized);
			lookQuat.eulerAngles = new Vector3 (rotationAngle.x,lookQuat.eulerAngles.y,rotationAngle.z);
			transform.rotation = lookQuat;
		}
		Physics.Raycast(transform.position,Vector3.down,out rayInfo);
		//transform.position = rayInfo.point + Vector3.up*(initHeight);
		moveObj.transform.position = rayInfo.point + Vector3.up*(initHeight);
	}
}
