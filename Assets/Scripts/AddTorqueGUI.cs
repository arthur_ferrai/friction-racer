﻿using UnityEngine;
using System.Collections;

public class AddTorqueGUI : IAddTorque {
	string selTorqueVal = "30";
	void OnGUI() {
		GUILayout.BeginHorizontal();
		GUILayout.Label("Max Torque");
		selTorqueVal = GUILayout.TextField(selTorqueVal);
		
		GUILayout.EndHorizontal();
		
		if (GUILayout.Button("Add Torque")){
			car.AddTorque(float.Parse(selTorqueVal));
		}
		
		if (GUILayout.Button("Stop")) {
			car.Stop();
		}
	}
}
