﻿using UnityEngine;
using System.Collections;

public class SetPoints : MonoBehaviour {

	public int points;
	public bool setNow = false;

	private AttributeRender r;
	void Start () {
		r = GetComponent<AttributeRender>() as AttributeRender;
	}
	void Update () {
		if (setNow) {
			setNow = false;
			r.SetAttributePoints(points);
		}
	}
}
