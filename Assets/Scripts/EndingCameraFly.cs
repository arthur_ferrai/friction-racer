﻿using UnityEngine;
using System.Collections;

public class EndingCameraFly : MonoBehaviour {
	public float timeToFadeIn = 1;
	public float fadeOutStart = 16;
	public TextMesh firstPlaceName,secondPlaceName,thirdPlaceName,lastPlaceName;

	public TextMesh gameOver;

	private TextMesh first, second, third, last;
	void Awake() {
		first  = transform.Find( "First Place").GetComponent<TextMesh>();
		second = transform.Find("Second Place").GetComponent<TextMesh>();
		third  = transform.Find( "Third Place").GetComponent<TextMesh>();
		last   = transform.Find(  "Last Place").GetComponent<TextMesh>();
	}
	void Start () {
		first.text  = "1st: " + firstPlaceName.text;
		second.text = "2nd: " + secondPlaceName.text;
		if (GameOptions.GetInt("GameType") == (int)GameType.single){
			third.text = "3rd: " + thirdPlaceName.text;
			last.text  = "4th: " + lastPlaceName.text;
		} else {
			third.text = (GameOptions.GetInt("Player3Active") == 1) ? "3rd: " + thirdPlaceName.text  : "";
			last.text  = (GameOptions.GetInt("Player4Active") == 1) ? "4th: " + lastPlaceName.text   : "";
		}
		PlayersAlternator p = GameObject.FindObjectOfType<PlayersAlternator>();
		p.enabled = false;
		iTween.CameraFadeAdd();
		iTween.CameraFadeTo(0,timeToFadeIn*Time.timeScale);
		float timeToShowPos = timeToFadeIn + 1f;
		iTween.MoveTo(gameOver.gameObject,iTween.Hash("x",0,
		                                              "delay",timeToFadeIn*Time.timeScale,
		                                              "time",2*Time.timeScale,
		                                              "easetype",iTween.EaseType.easeOutExpo,
		                                              "islocal",true));
		iTween.MoveTo(first.gameObject,iTween.Hash("x",0,
		                                           "delay",timeToShowPos*Time.timeScale,
		                                           "time",2*Time.timeScale,
		                                           "easetype",iTween.EaseType.easeOutExpo,
		                                           "islocal",true));
		iTween.MoveTo(second.gameObject,iTween.Hash("x",0,
		                                            "delay",(0.1f+timeToShowPos)*Time.timeScale,
		                                            "time",2*Time.timeScale,
		                                            "easetype",iTween.EaseType.easeOutExpo,
		                                            "islocal",true));
		iTween.MoveTo(third.gameObject,iTween.Hash("x",0,
		                                           "delay",(0.2f+timeToShowPos)*Time.timeScale,
		                                           "time",2*Time.timeScale,
		                                           "easetype",iTween.EaseType.easeOutExpo,
		                                           "islocal",true));
		iTween.MoveTo(last.gameObject,iTween.Hash("x",0,
		                                          "delay",(0.3f+timeToShowPos)*Time.timeScale,
		                                          "time",2*Time.timeScale,
		                                          "easetype",iTween.EaseType.easeOutExpo,
		                                          "islocal",true));
	}
	void Update(){
		//if(Input.touchCount > 0 || Input.GetMouseButton(0)) EndFinish();
	}
	public void EndFinish () {
		iTween.CameraFadeTo(iTween.Hash("amount",1,
		                                "time",timeToFadeIn*Time.timeScale,
		                                "oncomplete","BackToMainMenu",
		                                "oncompletetarget",gameObject));
	}
	void BackToMainMenu() {
		Application.LoadLevel("main menu");
	}
}
