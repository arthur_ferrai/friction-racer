﻿using UnityEngine;
using System.Collections;

public class PlayMusic : MonoBehaviour {
	public AudioClip music;

	private AudioSource bgMusic;
	void Awake () {
		GameObject obj = GameObject.Find("BG Music");
		if (obj == null) {
			bgMusic = (new GameObject("BG Music",typeof(AudioSource))).audio;
			bgMusic.playOnAwake = true;
			bgMusic.loop = true;
			DontDestroyOnLoad(bgMusic);
		} else {
			bgMusic = obj.audio;
		}
	}
	void Start() {
		if (bgMusic.clip != music){
			bgMusic.clip = music;
			bgMusic.Play();
		}
	}
}
