﻿using UnityEngine;
using System.Collections;

public class AttributesManager : MonoBehaviour {
	[System.Serializable]
	public class AttributeInfo {
		public int Acceleration;
		public int Weight;
	};

	public AttributeRender accelAttribute, weightAttribute;
	public AttributeInfo [] carsInfo;

	private int previous = -1;
	private ChildSelector selector;
	void Start () {
		selector = GameObject.FindObjectOfType<ChildSelector>();
	}
	void Update () {
		if (selector.Selected != previous){
			previous = selector.Selected;
			accelAttribute.SetAttributePoints(carsInfo[previous].Acceleration);
			weightAttribute.SetAttributePoints(carsInfo[previous].Weight);
		}
	}
}
