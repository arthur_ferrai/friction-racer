﻿using UnityEngine;
using System.Collections;

public class SpriteRepeater : MonoBehaviour {
	public Sprite tile;
	public Vector2 gridSize;
	public int sortingOrder;
	// Use this for initialization
	void Awake () {
		transform.position = Vector3.Scale(Camera.main.ScreenToWorldPoint(Vector3.zero), (Vector3.right + Vector3.up));
		float w = tile.rect.width, h = tile.rect.height;
		for (int y = 0; y < gridSize.y; y++) {
			for (int x = 0; x < gridSize.x; x++) {
				GameObject go = new GameObject();
				go.name = "SpriteRepeater";
				SpriteRenderer rend;
				rend = go.AddComponent<SpriteRenderer>() as SpriteRenderer;
				rend.sprite = tile;
				rend.sortingOrder = sortingOrder;
				go.transform.parent = transform;
				go.transform.localPosition = new Vector3(x*w + w/2, y*(h - 1) + h/2);
			}
		}
	}
}
