﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class FrictionCar : MonoBehaviour {

	public WheelCollider [] wheels;

	public float upFactor = 0.05f;
	public float downFactor = 0.005f;

	public float brakeTorque = 100;

	public Transform centerOfMass;

	private float maxTorque = 0;

	private bool decel = true;

	void Start(){
		rigidbody.centerOfMass = centerOfMass.localPosition;
	}

	float wheelTorque = 0;
	void FixedUpdate () {
		float dotFactor = Vector3.Dot(transform.forward,rigidbody.velocity);
		if(wheelTorque > maxTorque - 0.1f) {
			decel = true;
		}
		if(!decel) {
			wheelTorque = Mathf.Lerp(wheelTorque,maxTorque,upFactor);
		}else if (wheelTorque > 0) {
			wheelTorque = Mathf.Lerp(wheelTorque,0,downFactor);
		} 
		foreach(var wheel in wheels){
			if (dotFactor < 0){
				wheel.brakeTorque = brakeTorque;
				wheel.motorTorque = 0;
			} else {
				wheel.brakeTorque = 0;
				wheel.motorTorque = wheelTorque;
			}
		}

	}

	public void AddTorque(float t) {
		decel = false;
		maxTorque = t;
	}

	public void Stop() {

		foreach(var wheel in wheels){
			wheel.motorTorque = 0;
		}
		rigidbody.velocity = Vector3.zero;
	}

}
