/*
 * Codigo baseado nas aulas do professor Reinaldo Ramos (masterrey@gmail.com)
 * e no script SmoothLookAt (http://wiki.unity3d.com/index.php/SmoothLookAt_CS)
 */

using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {
	// objeto que a câmera vai seguir
	public Transform target;
	// distância que a câmera vai ficar do objeto
	public float distance;

	public float height=1;

	public float movementSpeed=1;

	public float rotationSpeed=1;
	
	//public bool preventCollision = true;

	//public float lockDistance = 0.1f;
	
	// Update is called once per frame
	void LateUpdate () {
		//Vector3 olha = target.position - transform.position;
		Vector3 vai = (target.position - target.forward*(distance)) - (transform.position);
		Quaternion totalRot = Quaternion.LookRotation(target.position-transform.position);
		// faz a camera olhar pra nave
		
		//transform.forward = olha.normalized;
		
		//transform.LookAt(target.position, target.up);

		transform.rotation = Quaternion.Slerp (transform.rotation, totalRot, Time.smoothDeltaTime*rotationSpeed);
		//transform.up = target.up;
		// faz a camera chegar perto da nave
		//transform.position += transform.forward*(vai.magnitude-1)*Time.deltaTime;

		//transform.position += vai.normalized * (vai.magnitude) * Time.deltaTime;
		transform.position += vai.normalized*movementSpeed*Time.smoothDeltaTime;
		//transform.position = Vector3.Lerp(transform.position,transform.position + (vai.normalized*(vai.magnitude)),0.1f);
		
		//transform.position = (target.position - Vector3.back)*(olha.magnitude-10)*Time.deltaTime;
		transform.position = new Vector3 (transform.position.x, target.position.y+height, transform.position.z);
		/*RaycastHit hit;
		if(preventCollision){
			while(!Physics.Raycast(transform.position, Vector3.down, out hit, 100)){transform.position+=transform.up;}
		
			if (hit.distance < 5){
				//transform.position+=Vector3.up;
				transform.position += Vector3.Lerp(Vector3.zero,Vector3.up,Time.deltaTime);
			}
		}*/
	}
}
