﻿using UnityEngine;
using System.Collections;

public class CameraFade : MonoBehaviour {
	public float fadeTime = 1;

	void Start () {
		iTween.CameraFadeAdd();
		iTween.CameraFadeFrom(1,fadeTime * Time.timeScale);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
