Shader "Parabole/Unlit/OpaqueReflective"
{
	Properties
	{
		_MainTex ("Base", 2D) = "white" {}
		_Cube("Reflection Map", Cube) = "" {}
		_CubeMapStrength ("Reflection Strength", Float) = 1
	}
	
	SubShader
	{
		Tags { "RenderType"="Opaque" "BW"="TrueReflective" }
		
		Pass
		{
			CGPROGRAM
				#include "UnityCG.cginc"
				#pragma vertex vert
				#pragma fragment frag
				#pragma multi_compile LIGHTMAP_ON LIGHTMAP_OFF
				 
				struct v2f{
					//half4 color : COLOR;
					float4 pos : SV_POSITION;
					fixed2 uv[2] : TEXCOORD0;
					fixed3 normalDir : TEXCOORD3;
					fixed3 viewDir : TEXCOORD2;
				};
				
				
				fixed _CubeMapStrength;
				uniform samplerCUBE _Cube;   
				sampler2D _MainTex;
				fixed4 _MainTex_ST;
				
				#ifdef LIGHTMAP_ON
				fixed4 unity_LightmapST;
				sampler2D unity_Lightmap;
				#endif
				
				v2f vert(appdata_full v)
				{
					v2f o;
					fixed4x4 modelMatrix = _Object2World;
					fixed4x4 modelMatrixInverse = _World2Object; 
					o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
					o.uv[0] = TRANSFORM_TEX(v.texcoord, _MainTex);
					
					o.viewDir = (mul(modelMatrix, v.vertex) 
					   - fixed4(_WorldSpaceCameraPos, 1.0)).xyz;
					o.normalDir = normalize((
					   mul(fixed4(v.normal, 0.0), modelMatrixInverse)).xyz);
					
					#ifdef LIGHTMAP_ON
					o.uv[1] = v.texcoord1.xy * unity_LightmapST.xy + unity_LightmapST.zw;
					#endif
					
					//o.color = v.color;
					return o;
				}
				
				fixed4 frag(v2f i) : COLOR
				{
					fixed cubeStrength=1.0;
					fixed4 c = tex2D(_MainTex, i.uv[0]);// * i.color;
					fixed3 reflectedDir = reflect(i.viewDir, normalize(i.normalDir));
					
					#ifdef LIGHTMAP_ON
					c.rgb *= DecodeLightmap(tex2D(unity_Lightmap, i.uv[1]));
					cubeStrength=lerp(0.0,1.0,(c.r+c.g+c.b)*0.3333);
					#endif
					
					return c+texCUBE(_Cube, normalize(reflectedDir))*_CubeMapStrength*cubeStrength;
				}
			ENDCG
		}
		
	}
	FallBack "Parabole/Unlit/Opaque"
}
//
//Shader "Cg shader with reflection map" {
//   Properties {
//      _Cube("Reflection Map", Cube) = "" {}
//   }
//   SubShader {
//      Pass {   
//         CGPROGRAM
// 
//         #pragma vertex vert  
//         #pragma fragment frag 
// 
//         // User-specified uniforms
//         uniform samplerCUBE _Cube;   
// 
//         // The following built-in uniforms are also
//         // defined in "UnityCG.cginc", which could be #included 
//         uniform float4 unity_Scale; // w = 1/scale; see _World2Object
//         uniform float3 _WorldSpaceCameraPos;
//         uniform float4x4 _Object2World; // model matrix
//         uniform float4x4 _World2Object; // inverse model matrix 
// 
//         struct vertexInput {
//            float4 vertex : POSITION;
//            float3 normal : NORMAL;
//         };
//         struct vertexOutput {
//            float4 pos : SV_POSITION;
//            float3 normalDir : TEXCOORD0;
//            float3 viewDir : TEXCOORD1;
//         };
// 
//         vertexOutput vert(vertexInput input) 
//         {
//            vertexOutput output;
// 
//            float4x4 modelMatrix = _Object2World;
//            float4x4 modelMatrixInverse = _World2Object; 
//               // multiplication with unity_Scale.w is unnecessary 
//               // because we normalize transformed vectors
// 
//            output.viewDir = float3(mul(modelMatrix, input.vertex) 
//               - float4(_WorldSpaceCameraPos, 1.0));
//            output.normalDir = normalize(float3(
//               mul(float4(input.normal, 0.0), modelMatrixInverse)));
//            output.pos = mul(UNITY_MATRIX_MVP, input.vertex);
//            return output;
//         }
// 
//         float4 frag(vertexOutput input) : COLOR
//         {
//            float3 reflectedDir = 
//               reflect(input.viewDir, normalize(input.normalDir));
//            return texCUBE(_Cube, reflectedDir);
//         }
// 
//         ENDCG
//      }
//   }
//}